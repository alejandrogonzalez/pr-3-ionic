// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyA4JVU0Kb5SO49SVMCuKAaBK6aUicfKugM",
    authDomain: "practica03-634e1.firebaseapp.com",
    projectId: "practica03-634e1",
    storageBucket: "practica03-634e1.appspot.com",
    messagingSenderId: "788750012050",
    appId: "1:788750012050:web:482a98fe003f7cad0c204b",
    measurementId: "G-DVZMBM7CLG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
