import { Component } from '@angular/core';
import { Contact } from '../../database/contact';

import { AlbumsService } from '../../database/contact.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  contacts = [];


  constructor(private albumsService: AlbumsService) {}

  ngOnInit(){
    this.fetchContacts();
    let contactRes = this.albumsService.getContactList();
    contactRes.snapshotChanges().subscribe(res =>{
      this.contacts = [];
      res.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;
        this.contacts.push(a as Contact);
      })
    })

  }

  fetchContacts(){
    this.albumsService.getContactList().valueChanges().subscribe(res=>{
      console.log(res)
    })
  }

  deleteContact(id){
    if(window.confirm('¿Estás seguro de que quieres borrar este contacto?')){
      this.albumsService.deleteContact(id);
    }
  }

}




