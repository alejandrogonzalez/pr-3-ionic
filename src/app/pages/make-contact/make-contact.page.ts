import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { AlbumsService } from './../../database/contact.service';

@Component({
  selector: 'app-make-contact',
  templateUrl: './make-contact.page.html',
  styleUrls: ['./make-contact.page.scss'],
})
export class MakeContactPage implements OnInit {

  contactForm: FormGroup;

  constructor(private albumService: AlbumsService,
    private router: Router,
    public fb: FormBuilder) { }

  ngOnInit() {
    this.contactForm = this.fb.group({
      albumNom:[''],
      groupNom:[''],
      albumDuracion:[''],
      año:[''],
      genero:['']
    })
  }

  formSubmit(){
    if(!this.contactForm.valid){
      return false;
    }else{
      this.albumService.createContact(this.contactForm.value).then(res=>{
        this.contactForm.reset();
        this.router.navigate(['/home']);
      });
    }
  }
}
