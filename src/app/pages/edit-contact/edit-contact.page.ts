import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlbumsService } from '../../database/contact.service';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.page.html',
  styleUrls: ['./edit-contact.page.scss'],
})
export class EditContactPage implements OnInit {
  updateContactForm: FormGroup;
  id: any;

  constructor(private albumService: AlbumsService,private actRoute: ActivatedRoute,
    private router: Router, public fb: FormBuilder) {
      this.id = this.actRoute.snapshot.paramMap.get('id');
      this.albumService.getContact(this.id).valueChanges().subscribe(res=>{
        this.updateContactForm.setValue(res);
      })
     }

  ngOnInit() {
    this.updateContactForm = this.fb.group({
      albumNom:[''],
      groupNom:[''],
      albumDuracion:[''],
      año:[''],
      genero:['']
    })
  }

  updateForm(){
    this.albumService.updateContact(this.id, this.updateContactForm.value)
      .then(()=>{
        this.router.navigate(['/home']);
      });
  }

}
