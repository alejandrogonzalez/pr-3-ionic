import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditContactPageRoutingModule } from './edit-contact-routing.module';

import { EditContactPage } from './edit-contact.page';

import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { AlbumsService } from '../../database/contact.service';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditContactPageRoutingModule
  ],
  declarations: [EditContactPage]
})
export class EditContactPageModule {
  updateContactForm: FormGroup;
  id: any;

  constructor(private ContactService: AlbumsService,private actRoute: ActivatedRoute,
    private router: Router,public fb: FormBuilder){
      this.id = this.actRoute.snapshot.paramMap.get('id');
      this.ContactService.getContact(this.id).valueChanges().subscribe(res =>{
        this.updateContactForm.setValue(res);
      });
    }
}
