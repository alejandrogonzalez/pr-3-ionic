import { Injectable } from '@angular/core';
import { Contact } from '../database/contact';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
        

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  albumList: AngularFireList<any>;
  contact: AngularFireObject<any>;

  constructor(private db: AngularFireDatabase) { }

  //Create
  createContact(album: Contact){
    this.albumList = this.db.list('/albums');
    return this.albumList.push({
      nomGroup: album.nomGroup,
      nomAlbum: album.nomAlbum,
      duracion: album.duracion,
      año: album.año,
      genero: album.genero
    });
  }

  //Get Contact

  getContact(id: string){
    this.contact = this.db.object('/albums/' + id);
    return this.contact;
  }

  getalbumList(){
    this.albumList = this.db.list('/albums');
    return this.albumList;
  }

  //Update

  updateContact(id,newAlbum: Contact){
    return this.contact.update({
      nomGroup: newAlbum.nomGroup,
      nomAlbum:newAlbum.nomAlbum,
      duracion: newAlbum.duracion, 
      año: newAlbum.año, 
      genero: newAlbum.genero, 
    })
  }

  //Delete

  deleteContact(id: string){
    this.contact = this.db.object('/albums/' + id);
    this.contact.remove();
  }




}
