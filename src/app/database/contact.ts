export class Contact{
    $key: string;
    nomGroup: string;
    nomAlbum: string;
    duracion: number;
    año: number;
    genero: string;
}